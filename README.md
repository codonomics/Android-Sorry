Android-Sorry
======

Sorry! (board game)

Sorry is an Android application, created to spice up the classic Sorry! board game. This app allows users to play with the classic card deck as well as play with a dynamic deck. In dynamic deck, the cards are randomly generated as users play the game. For example: users don't always draw 11 or switch anymore, they could draw things like 12 backwards, 3 or split, etc...

Game had 1,000 - 5,000 download on Android. However, it was recently deleted because of (Hasbro) copyright violations. 