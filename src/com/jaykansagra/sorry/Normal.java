package com.jaykansagra.sorry;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Normal extends Activity implements OnClickListener {

	private ArrayList<String> arr1 = new ArrayList<String>();
	private Button next;
	private int count;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game);
		next = (Button) findViewById(R.id.NEXT);
		next.setOnClickListener(this);
		
		arr1.add("Move 1 forward \nOR \nSTART");
		arr1.add("Move 2 forward \nOR \nSTART \n\nAND\nTake another turn");
		arr1.add("Move 3 forward");
		arr1.add("Move 4 backward");
		arr1.add("Move 5 forward");
		arr1.add("Move 7 forward \nOR \nSPLIT");
		arr1.add("Move 8 forward");
		arr1.add("Move 10 forward");
		arr1.add("Move 11 forward \nOR \nSWITCH");
		arr1.add("Move 12 forward");
		arr1.add("SORRY!");
		
		count = (int) (Math.random()*4);
	}
	
	public void onClick(View v) {
		switch (count) {
		case 0: findViewById(R.id.RL).setBackgroundColor(Color.argb(255, 200, 0, 0)); break;
		case 1: findViewById(R.id.RL).setBackgroundColor(Color.argb(255, 0, 100, 255)); break;
		case 2: findViewById(R.id.RL).setBackgroundColor(Color.argb(255, 255, 150, 0)); break;
		case 3: findViewById(R.id.RL).setBackgroundColor(Color.argb(255, 0, 175, 0)); break;
		}
		
		int random = (int) (Math.random()*arr1.size());

		((TextView) findViewById(R.id.TextView)).setText(arr1.get(random));
		
		if(random != 1){
			next();	
		}
	}
	
	public void next() {
		if(count > 2)
			count = 0;
		else 
			count++;
	}
}
