package com.jaykansagra.sorry;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Spicy extends Activity implements OnClickListener {

	private ArrayList<String> arr1 = new ArrayList<String>();
	private ArrayList<String> arr2 = new ArrayList<String>();
	private ArrayList<String> arr3 = new ArrayList<String>();
	private Button next;
	private int count;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game);
		next = (Button) findViewById(R.id.NEXT);
		next.setOnClickListener(this);
		
		arr1.add("Move 1 ");
		arr1.add("Move 2 ");
		arr1.add("Move 3 ");
		arr1.add("Move 4 ");
		arr1.add("Move 5 ");
		arr1.add("Move 7 ");
		arr1.add("Move 8 ");
		arr1.add("Move 10 ");
		arr1.add("Move 11 ");
		arr1.add("Move 12 ");
		arr1.add("SORRY!");
		
		arr2.add("forward");
		arr2.add("forward");
		arr2.add("forward");
		arr2.add("backward");
		
		arr3.add("\nOR\nSTART");
		arr3.add("\nOR\nSTART");
		arr3.add("\nOR\nSWITCH");
		arr3.add("\nOR\nSWITCH");
		arr3.add("\nOR\nSPLIT");
		arr3.add("\nOR\nSPLIT");
		arr3.add("\nAND\nTake another turn");
		arr3.add("");
		arr3.add("");
		arr3.add("");
		arr3.add("");
		arr3.add("");
		arr3.add("");
		
		count = (int) (Math.random()*4);
	}
	
	public void onClick(View v) {
		switch (count) {
		case 0: findViewById(R.id.RL).setBackgroundColor(Color.argb(255, 200, 0, 0)); break;
		case 1: findViewById(R.id.RL).setBackgroundColor(Color.argb(255, 0, 100, 255)); break;
		case 2: findViewById(R.id.RL).setBackgroundColor(Color.argb(255, 255, 150, 0)); break;
		case 3: findViewById(R.id.RL).setBackgroundColor(Color.argb(255, 0, 175, 0)); break;
		}
		
		int rand1 = (int) (Math.random()*arr1.size());
		int rand2 = (int) (Math.random()*arr2.size());
		int rand3 = (int) (Math.random()*arr3.size());
		
		if(rand1 < 1) {
			((TextView) findViewById(R.id.TextView)).setText(arr1.get(arr1.size()-1));
		}
		else {
			((TextView) findViewById(R.id.TextView)).setText(
					arr1.get(rand1-1) +
					arr2.get(rand2) +
					arr3.get(rand3));
		}
		
		if(rand3 != 6)
			next();	
	}
	
	public void next() {
		if(count > 2)
			count = 0;
		else 
			count++;
	}
}
